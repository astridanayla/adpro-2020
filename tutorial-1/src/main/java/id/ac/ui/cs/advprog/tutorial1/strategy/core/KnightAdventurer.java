package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
        private String alias;

        public KnightAdventurer(){
                this.alias = "Knight";

                AttackWithSword sword = new AttackWithSword();
                DefendWithArmor armor = new DefendWithArmor();

                setAttackBehavior(sword);
                setDefenseBehavior(armor);
        }

        public String getAlias(){
                return this.alias;
        }

}
