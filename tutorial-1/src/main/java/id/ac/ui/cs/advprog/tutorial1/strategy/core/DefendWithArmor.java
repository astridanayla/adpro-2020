package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    private String type;

    public DefendWithArmor(){
        this.type = "Armor";
    }

    public String getType(){
        return this.type;
    }

    public String defend(){
        return "Defended with armor";
    }
}
