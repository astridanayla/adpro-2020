package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    private String type;

    public DefendWithShield(){
        this.type = "Shield";
    }

    public String getType(){
        return this.type;
    }

    public String defend(){
        return "Defended with shield";
    }
}
