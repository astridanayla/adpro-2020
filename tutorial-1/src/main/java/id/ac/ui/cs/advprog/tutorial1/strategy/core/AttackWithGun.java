package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    private String type;

    public AttackWithGun(){
        this.type = "Gun";
    }

    public String getType(){
        return this.type;
    }

    public String attack(){
        return "Attacked with gun";
    }
}
