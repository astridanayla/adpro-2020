package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
        private String alias;

        public MysticAdventurer(){
            this.alias = "Mystic";

            AttackWithMagic magic = new AttackWithMagic();
            DefendWithShield shield = new DefendWithShield();

            setAttackBehavior(magic);
            setDefenseBehavior(shield);
        }

        public String getAlias(){
            return this.alias;
        }
}
