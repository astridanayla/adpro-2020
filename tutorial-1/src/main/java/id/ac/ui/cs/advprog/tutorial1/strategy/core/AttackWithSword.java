package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    private String type;

    public AttackWithSword(){
        this.type = "Sword";
    }

    public String getType(){
        return this.type;
    }

    public String attack(){
        return "Attacked with sword";
    }
}
