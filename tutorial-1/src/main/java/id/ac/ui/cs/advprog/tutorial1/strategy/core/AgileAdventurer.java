package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
        private String alias;

        public AgileAdventurer(){
            this.alias = "Agile";

            AttackWithGun gun = new AttackWithGun();
            DefendWithBarrier barrier = new DefendWithBarrier();

            setAttackBehavior(gun);
            setDefenseBehavior(barrier);
        }

        public String getAlias(){
            return this.alias;
        }


}
