package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    private String type;

    public AttackWithMagic(){
        this.type = "Magic";
    }

    public String getType(){
        return this.type;
    }

    public String attack(){
        return "Attacked with magic";
    }
}
