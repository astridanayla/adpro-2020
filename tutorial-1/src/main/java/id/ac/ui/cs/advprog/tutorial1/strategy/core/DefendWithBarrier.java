package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    private String type;

    public DefendWithBarrier(){
        this.type = "Barrier";
    }

    public String getType(){
        return this.type;
    }

    public String defend(){
        return "Defended with barrier";
    }
}
