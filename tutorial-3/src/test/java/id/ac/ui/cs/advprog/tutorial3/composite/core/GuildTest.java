package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        Member test = new OrdinaryMember("Astrid", "Mahasiswa");
        guildMaster.addChildMember(test);
        assertEquals(true, guildMaster.getChildMembers().contains(test));
    }

    @Test
    public void testMethodRemoveMember() {
        Member test = new OrdinaryMember("Astrid", "Mahasiswa");
        guildMaster.addChildMember(test);
        guildMaster.removeChildMember(test);
        assertEquals(false, guildMaster.getChildMembers().contains(test));
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member test = new OrdinaryMember("Astrid", "Mahasiswa");
        guildMaster.addChildMember(test);
        assertEquals(test, guild.getMember("Astrid", "Mahasiswa"));
    }
}
