package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;


import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;
    @BeforeEach
    public void setUp(){
        chaosUpgrade = new ChaosUpgrade(new Gun());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Gun", chaosUpgrade.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        assertEquals("Automatic Gun upgraded with Chaos Upgrade", chaosUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        int randomValue = chaosUpgrade.getWeaponValue() - 20;
        assertTrue(50 <= randomValue && randomValue <= 55);
    }

}
