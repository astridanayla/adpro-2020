package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Sword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegularUpgradeTest {

    private RegularUpgrade regularUpgrade;

    @BeforeEach
    public void setUp(){
        regularUpgrade = new RegularUpgrade(new Sword());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Sword", regularUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals("Great Sword upgraded with Regular Upgrade", regularUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        int randomValue = regularUpgrade.getWeaponValue() - 25;
        assertTrue(1 <= randomValue && randomValue <= 5);
    }
}
