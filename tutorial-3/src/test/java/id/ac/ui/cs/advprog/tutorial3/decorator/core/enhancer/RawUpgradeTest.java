package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RawUpgradeTest {

    private RawUpgrade rawUpgrade;

    @BeforeEach
    public void setUp(){
        rawUpgrade = new RawUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Shield", rawUpgrade.getName());
    }

    @Test
    public void testMethodGetDescription(){
        assertEquals("Heater Shield upgraded with Raw Upgrade", rawUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        int randomValue = rawUpgrade.getWeaponValue() - 10;
        assertTrue(5 <= randomValue && randomValue <= 10);
    }
}
