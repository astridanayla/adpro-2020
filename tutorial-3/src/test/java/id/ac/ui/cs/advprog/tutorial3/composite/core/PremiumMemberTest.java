package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        Member test = new PremiumMember("Astrid", "Mahasiswa");
        member.addChildMember(test);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member test = new PremiumMember("Astrid", "Mahasiswa");
        member.addChildMember(test);
        member.removeChildMember(test);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member test1 = new OrdinaryMember("Astrid", "Mahasiswa");
        Member test2 = new OrdinaryMember("Mahdia", "Mahasiswa");
        Member test3 = new OrdinaryMember("Nasywa", "Mahasiswa");
        Member test4 = new OrdinaryMember("Ayasha", "Mahasiswa");
        member.addChildMember(test1);
        member.addChildMember(test2);
        member.addChildMember(test3);
        member.addChildMember(test4);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member masterTest = new PremiumMember("Thina", "Guild Master");
        Member test1 = new OrdinaryMember("Astrid", "Mahasiswa");
        Member test2 = new OrdinaryMember("Mahdia", "Mahasiswa");
        Member test3 = new OrdinaryMember("Nasywa", "Mahasiswa");
        Member test4 = new OrdinaryMember("Ayasha", "Mahasiswa");
        masterTest.addChildMember(test1);
        masterTest.addChildMember(test2);
        masterTest.addChildMember(test3);
        masterTest.addChildMember(test4);
        assertEquals(4, masterTest.getChildMembers().size());
    }
}
