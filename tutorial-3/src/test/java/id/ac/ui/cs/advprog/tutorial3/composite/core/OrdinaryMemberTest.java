package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Nina", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Merchant", member.getRole());
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        Member test = new OrdinaryMember("Astrid", "Mahasiswa");
        member.addChildMember(test);
        assertTrue(member.getChildMembers().size() == 0);
        member.removeChildMember(test);
        assertTrue(member.getChildMembers().size() == 0);
    }


}
