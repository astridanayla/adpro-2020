package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import java.util.Random;

public abstract class Weapon {

    protected String weaponName;
    protected String weaponDescription;
    protected int weaponValue;

    public String getName(){
        return weaponName;
    }

    public int getWeaponValue(){
        return weaponValue;
    }

    public String getDescription(){
        return weaponDescription;
    }

    public static int randomRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }
}
