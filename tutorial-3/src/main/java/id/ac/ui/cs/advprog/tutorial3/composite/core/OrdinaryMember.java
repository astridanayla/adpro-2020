package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.*;

public class OrdinaryMember implements Member {
    private String name;
    private String role;
    private List<Member> childMembers;

    public OrdinaryMember(String name, String role){
        this.name = name;
        this.role = role;
        this.childMembers = new ArrayList<Member>();
    }

    public String getName(){
        return this.name;
    }

    public String getRole(){
        return this.role;
    }

    public void addChildMember(Member member){}

    public void removeChildMember(Member member){}

    public List<Member> getChildMembers(){
        return this.childMembers;
    }
}
